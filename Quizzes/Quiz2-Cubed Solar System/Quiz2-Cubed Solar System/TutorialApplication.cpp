/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Creating of planets and setting the revolution speed
	//Sun 
	mPlanets.push_back(Planet::createPlanet(*mSceneMgr, 20, ColourValue(1, 0.82, 0.2)));
	mPlanets[0]->getSceneNode()->setPosition(0, 0, 0);

	//Mercury
	mPlanets.push_back(Planet::createPlanet(*mSceneMgr, 3, ColourValue(0.48, 0.51, 0.47)));
	mPlanets[1]->getSceneNode()->setPosition(0, 0, -200);
	mPlanets[1]->calculateSpeed(88.0f);

	//Venus
	mPlanets.push_back(Planet::createPlanet(*mSceneMgr, 5, ColourValue(0.8, 0.38, 0.18)));
	mPlanets[2]->getSceneNode()->setPosition(0, 0, -400);
	mPlanets[2]->calculateSpeed(224.0f);

	//Earth
	mPlanets.push_back(Planet::createPlanet(*mSceneMgr, 10, ColourValue(0,1,1)));
	mPlanets[3]->getSceneNode()->setPosition(0, 0, -600);
	mPlanets[3]->calculateSpeed(365.0f);

	//Moon
	mPlanets.push_back(Planet::createPlanet(*mSceneMgr, 1, ColourValue(0.48, 0.51, 0.47)));
	mPlanets[4]->getSceneNode()->setPosition(0, 0, -650);
	mPlanets[4]->calculateSpeed(5.0f);

	//Mars
	mPlanets.push_back(Planet::createPlanet(*mSceneMgr, 8, ColourValue(1,0,0)));
	mPlanets[5]->getSceneNode()->setPosition(0, 0, -800);
	mPlanets[5]->calculateSpeed(687.0f);
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	// Rotation of the planets, moon, and sun
	for (int c = 0; c <= 5; c++)
	{
		mPlanets[c]->rotate(evt.timeSinceLastFrame);
	}

	// Revolution of the planets around the sun
	// Mercury
	mPlanets[1]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);
	// Venus
	mPlanets[2]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);
	// Earth
	mPlanets[3]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);
	// Mars
	mPlanets[5]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);

	// Revolution of the moon around the earth
	mPlanets[4]->orbit(mPlanets[3]->getSceneNode(), evt.timeSinceLastFrame);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
