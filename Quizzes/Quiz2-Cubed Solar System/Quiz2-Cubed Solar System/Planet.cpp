#include "Planet.h"



Planet::Planet(SceneNode* sceneNode)
{
	mSceneNode = sceneNode;
}


Planet::~Planet()
{
}

SceneNode * Planet::getSceneNode()
{
	return mSceneNode;
}

Planet * Planet::createPlanet(SceneManager & sceneManager, float size, ColourValue colour)
{
	// Ask the scene manager to create an object for us
	ManualObject* manual = sceneManager.createManualObject();
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	manual->colour(colour);

	// Define the vertices
	manual->position(-size / 2, -size / 2, -size / 2); // 0
	manual->position(size / 2, -size / 2, -size / 2); // 1
	manual->position(size / 2, size / 2, -size / 2); // 2
	manual->position(-size / 2, size / 2, -size / 2); // 3
	manual->position(-size / 2, -size / 2, size / 2); // 4
	manual->position(size / 2, -size / 2, size / 2); // 5
	manual->position(size / 2, size / 2, size / 2); // 6
	manual->position(-size / 2, size / 2, size / 2); // 7

	// Define order of vertices (define buffer)
	// Front
	manual->index(2);
	manual->index(1);
	manual->index(0);

	manual->index(2);
	manual->index(0);
	manual->index(3);

	// Back
	manual->index(6);
	manual->index(7);
	manual->index(5);

	manual->index(7);
	manual->index(4);
	manual->index(5);

	// Right
	manual->index(6);
	manual->index(5);
	manual->index(2);

	manual->index(5);
	manual->index(1);
	manual->index(2);

	// Left
	manual->index(0);
	manual->index(4);
	manual->index(7);

	manual->index(7);
	manual->index(3);
	manual->index(0);

	// Top
	manual->index(3);
	manual->index(7);
	manual->index(2);

	manual->index(7);
	manual->index(6);
	manual->index(2);

	// Bottom
	manual->index(5);
	manual->index(4);
	manual->index(0);

	manual->index(0);
	manual->index(1);
	manual->index(5);

	// End drawing
	manual->end();
	
	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode();
	node->attachObject(manual);
	Planet* planet = new Planet(node);

	return planet;
}

void Planet::calculateSpeed(float speed)
{
	// Formula for the revolution of planets
	float pi = 3.141592654f;
	mOrbitSpeed = 365.0f / speed;
	mOrbitSpeed *= (6.0f*pi) / 180.0f;
}

void Planet::rotate(float speed)
{
	mSceneNode->rotate(Ogre::Vector3(0, 1, 0), Radian(speed));
}

void Planet::orbit(SceneNode * origin, float speed)
{
	float newX = (mSceneNode->getPosition().x - origin->getPosition().x) * cos(speed * mOrbitSpeed) + (mSceneNode->getPosition().z - origin->getPosition().z) * sin(speed * mOrbitSpeed);
	float newZ = (mSceneNode->getPosition().x - origin->getPosition().x) * -sin(speed * mOrbitSpeed) + (mSceneNode->getPosition().z - origin->getPosition().z) * cos(speed * mOrbitSpeed);

	// Allocating the new coordinates into a variable. Y remains the same.
	float x = newX + origin->getPosition().x;
	float y = mSceneNode->getPosition().y;
	float z = newZ + origin->getPosition().z;

	// Setting the new coordinates
	mSceneNode->setPosition(x, y, z);
}
