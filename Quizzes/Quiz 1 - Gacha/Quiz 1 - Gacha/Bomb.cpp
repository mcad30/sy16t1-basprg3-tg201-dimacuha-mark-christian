#include "Bomb.h"



Bomb::Bomb() : Item("Bomb")
{

}


Bomb::~Bomb()
{

}

void Bomb::itemEffect(Unit * player)
{	
	cout << "You pulled a bomb, -25HP" << endl;
	player->setDmg();
	player->minusCrystals();
}
