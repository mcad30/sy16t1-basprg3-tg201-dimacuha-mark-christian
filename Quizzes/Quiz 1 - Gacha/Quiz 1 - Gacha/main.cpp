#include "Unit.h"
#include "Gacha.h"
#include "Bomb.h"
#include "Potion.h"
#include "Crystals.h"
#include "Rare.h"
#include "SuperRare.h"
#include "SuperSuperRare.h"
#include <time.h>

int main()
{
	srand(time(0));
	Item* sSRare = new SuperSuperRare();
	Item* sRare = new SuperRare();
	Item* rare = new Rare();
	Item* potion = new Potion();
	Item* bomb = new Bomb();
	Item* crystals = new Crystals();
	Unit* player = new Unit("Macky");

	int pull = 0;

	for (;;)
	{

		cout << "HP: " << player->getHp() << endl;
		cout << "Rarity Points: " << player->getRarityPoints() << endl;
		cout << "Crystals: " << player->getCrystals() << endl;
		cout << "Pulls: " << pull << endl;
		system("pause");
		system("cls");
		
		int random = rand() % 100 + 1;

		if (random == 1) // 1% Super Super Rare
		{
			player->pullItem(sSRare, player);
			system("pause");
			system("cls");
		}

		else if (random >= 2 && random <= 10) // 9% Super Rare
		{
			player->pullItem(sRare, player);
			system("pause");
			system("cls");
		}
		else if (random >= 11 && random <= 50) // 40% Rare
		{
			player->pullItem(rare, player);
			system("pause");
			system("cls");
		}
		else if (random >= 51 && random <= 65) // 15% Potion
		{
			player->pullItem(potion, player);
			system("pause");
			system("cls");
		}
		else if (random >= 66 && random <= 85) // 20% Bomb
		{
			player->pullItem(bomb, player);
			system("pause");
			system("cls");
		}
		else if (random >= 86 && random <= 100) // 15% Crystals
		{
			player->pullItem(crystals, player);
			system("pause");
			system("cls");
		}
		pull++;

		if (player->getHp() > 0 && player->getCrystals() > 0 && player->getRarityPoints() >= 100)
		{
			cout << "HP: " << player->getHp() << endl;
			cout << "Rarity Points: " << player->getRarityPoints() << endl;
			cout << "Crystals: " << player->getCrystals() << endl;
			cout << "Pulls: " << pull << endl;
			cout << "==============================================" << endl;
			cout << "YOU WIN!" << endl;
			cout << "==============================================" << endl;
			player->printResults();

			break;
		}

		else if (player->getHp() == 0 || player->getCrystals() == 0)
		{
			cout << "HP: " << player->getHp() << endl;
			cout << "Rarity Points: " << player->getRarityPoints() << endl;
			cout << "Crystals: " << player->getCrystals() << endl;
			cout << "Pulls: " << pull << endl;
			cout << "==============================================" << endl;
			cout << "GAME OVER!" << endl;
			cout << "==============================================" << endl;
			player->printResults();
			break;
		}
		
	}
	delete sSRare; sSRare = NULL;
	delete sRare; sRare = NULL;
	delete rare; rare = NULL;
	delete bomb; bomb = NULL;
	delete potion; potion = NULL;
	delete crystals; crystals = NULL;
	delete player; player = NULL;

	system("pause");
	return 0;
}