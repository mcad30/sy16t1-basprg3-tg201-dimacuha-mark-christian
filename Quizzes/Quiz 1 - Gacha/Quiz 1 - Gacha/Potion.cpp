#include "Potion.h"



Potion::Potion() : Item("Potion")
{

}


Potion::~Potion()
{
}

void Potion::itemEffect(Unit * player)
{	
	cout << "You pulled a potion, +30HP" << endl;
	player->setHp();
	player->minusCrystals();
}
