#include "Unit.h"



Unit::Unit(string name)
{
	mName = name;
	mHp = 100;
	mRarityPoints = 0;
	mCrystals = 100;
	mSuperSuperRareCount = 0;
	mSuperRareCount = 0;
	mRarecount = 0;
	mPotionCount = 0;
	mCrystalCount = 0;
	mBombCount = 0;
}


Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getRarityPoints()
{
	return mRarityPoints;
}

int Unit::getCrystals()
{
	return mCrystals;
}

void Unit::setHp()
{
	mHp += 30;
	mPotionCount++;
}

void Unit::setCrystals()
{
	mCrystalCount++;
	mCrystals += 15;
}

void Unit::setDmg()
{
	mBombCount++;
	mHp -= 25;
	if (mHp < 0)
	{
		mHp = 0;

	}
}

void Unit::minusCrystals()
{
	mCrystals -= 5;
}

void Unit::superSuperRarePulled(int amount)
{
	mRarityPoints += amount;
	mSuperSuperRareCount++;
}

void Unit::superRarePulled(int amount)
{
	mRarityPoints += amount;
	mSuperRareCount++;
}

void Unit::rarePulled(int amount)
{
	mRarityPoints += amount;
	mRarecount++;
}

void Unit::pullItem(Item * gacha, Unit* player)
{
	gacha->itemEffect(player);
}

void Unit::printResults()
{
	cout << "Potions pulled: " << mPotionCount << endl;
	cout << "Bombs pulled: " << mBombCount << endl;
	cout << "Crystals pulled: " << mCrystalCount << endl;
	cout << "Super Super Rares pulled: " << mSuperSuperRareCount << endl;
	cout << "Super Rares pulled: " << mSuperRareCount << endl;
	cout << "Rares pulled: " << mRarecount << endl;
}

