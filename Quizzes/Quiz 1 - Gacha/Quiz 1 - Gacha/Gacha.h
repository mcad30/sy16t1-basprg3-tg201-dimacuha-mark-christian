#pragma once
#include "Unit.h"

class Item
{
public:
	Item(string name);
	~Item();

	virtual void itemEffect(Unit* player);

private:

	string mName;
};

