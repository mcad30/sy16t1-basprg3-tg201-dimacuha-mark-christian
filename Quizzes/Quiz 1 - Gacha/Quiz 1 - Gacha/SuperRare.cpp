#include "SuperRare.h"



SuperRare::SuperRare() : Item("Super Rare")
{
}


SuperRare::~SuperRare()
{
}

void SuperRare::itemEffect(Unit * player)
{	
	cout << "You pulled a SUPER RARE!, +10 RP! " << endl;
	player->superRarePulled(10);
	player->minusCrystals();
}
