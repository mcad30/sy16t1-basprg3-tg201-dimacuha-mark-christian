#include "Crystals.h"



Crystals::Crystals() : Item("Crystals")
{

}


Crystals::~Crystals()
{

}

void Crystals::itemEffect(Unit * player)
{	
	cout << "You pulled a crystal, +15 Crystals" << endl;
	player->setCrystals();
	player->minusCrystals();
}
