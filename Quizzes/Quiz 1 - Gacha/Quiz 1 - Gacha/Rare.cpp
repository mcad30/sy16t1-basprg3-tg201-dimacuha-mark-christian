#include "Rare.h"



Rare::Rare() : Item("Rare")
{
}


Rare::~Rare()
{
}

void Rare::itemEffect(Unit * player)
{	
	cout << "You pulled a RARE!, +1 RP! " << endl;
	player->rarePulled(1);
	player->minusCrystals();
}


