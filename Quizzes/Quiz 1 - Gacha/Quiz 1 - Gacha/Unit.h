#pragma once
#include <string>
#include <iostream>

using namespace std;

class Item;

class Unit
{
public:
	Unit(string name);
	~Unit();
	string getName();
	int getHp();
	int getRarityPoints();
	int getCrystals();
	void setHp(); // Potion pulled
	void setCrystals(); // Crystal pulled
	void setDmg(); // Bomb pulled
	void superSuperRarePulled(int amount); // SSR Pulled
	void superRarePulled(int amount); // SR pulled
	void rarePulled(int amount); // R pulled
	void minusCrystals();
	void pullItem(Item* gacha, Unit* player);
	void printResults();

private:

	string mName;
	int mHp;
	int mCrystals;
	int mRarityPoints;
	int mSuperSuperRareCount;
	int mSuperRareCount;
	int mRarecount;
	int mPotionCount;
	int mCrystalCount;
	int mBombCount;
};

#include "Gacha.h"