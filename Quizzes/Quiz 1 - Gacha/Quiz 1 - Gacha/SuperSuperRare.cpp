#include "SuperSuperRare.h"



SuperSuperRare::SuperSuperRare() : Item("Super Super Rare")
{
}


SuperSuperRare::~SuperSuperRare()
{
}

void SuperSuperRare::itemEffect(Unit * player)
{	
	cout << "You pulled a SUPER SUPER RARE!, +50 RP! " << endl;
	player->superSuperRarePulled(50);
	player->minusCrystals();
}
