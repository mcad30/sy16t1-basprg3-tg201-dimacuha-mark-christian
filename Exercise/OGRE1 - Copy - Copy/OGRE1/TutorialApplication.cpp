/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	mSpeed = 0;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject* TutorialApplication::createCube(float size)
{
	// Ask the scene manager to create an object for us
	ManualObject* manual = mSceneMgr->createManualObject();
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Define the vertices
	manual->position(-size/2, -size/2, -size/2); // 0
	manual->position(size/2, -size/2, -size/2); // 1
	manual->position(size/2, size/2, -size/2); // 2
	manual->position(-size/2, size/2, -size/2); // 3
	manual->position(-size/2, -size/2, size/2); // 4
	manual->position(size/2, -size/2, size/2); // 5
	manual->position(size/2, size/2, size/2); // 6
	manual->position(-size/2, size/2, size/2); // 7

	// Define order of vertices (define buffer)

	// Front
	manual->index(2);
	manual->index(1);
	manual->index(0);
	manual->index(2);
	manual->index(0);
	manual->index(3);

	// Back
	manual->index(6);
	manual->index(7);
	manual->index(5);
	manual->index(7);
	manual->index(4);
	manual->index(5);

	// Right
	manual->index(6);
	manual->index(5);
	manual->index(2);
	manual->index(5);
	manual->index(1);
	manual->index(2);

	// Left
	manual->index(0);
	manual->index(4);
	manual->index(7);
	manual->index(7);
	manual->index(3);
	manual->index(0);

	// Top
	manual->index(3);
	manual->index(7);
	manual->index(2);
	manual->index(7);
	manual->index(6);
	manual->index(2);

	// Bottom
	manual->index(5);
	manual->index(4);
	manual->index(0);
	manual->index(0);
	manual->index(1);
	manual->index(5);

	// End drawing
	manual->end();
	return manual;
}



//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	//mCamera->setPolygonMode(PolygonMode::PM_WIREFRAME);

	ManualObject* manual = createCube(10.0f);
	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mObject->attachObject(manual);


	ManualObject* manual2 = createCube(10.0f);
	mRevolvingCube = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mRevolvingCube->attachObject(manual2);
	mRevolvingCube->translate(30, 0, 0);

	// Attach the manual object to the scene
}
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	mSpeed += evt.timeSinceLastFrame;
	Degree revolvingDegree = Degree(45.0f * evt.timeSinceLastFrame);
	float newX = (mRevolvingCube->getPosition().x * Math::Cos(Radian(revolvingDegree))) + (mRevolvingCube->getPosition().z * Math::Sin(Radian(revolvingDegree)));
	float newZ = (mRevolvingCube->getPosition().x * -Math::Sin(Radian(revolvingDegree))) + (mRevolvingCube->getPosition().z * Math::Cos(Radian(revolvingDegree)));

	mRevolvingCube->setPosition(newX, 0, newZ);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
