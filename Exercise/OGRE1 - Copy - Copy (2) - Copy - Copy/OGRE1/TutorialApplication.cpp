/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	mSpeed = 0;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject* TutorialApplication::createCube(float size)
{
	// Ask the scene manager to create an object for us
	ManualObject* manual = mSceneMgr->createManualObject();
	MaterialPtr myManualMaterialObject = MaterialManager::getSingleton().create("manualMaterial", "General");

	myManualMaterialObject->getTechnique(0)->setLightingEnabled(true);
	myManualMaterialObject->getTechnique(0)->setDiffuse(0, 1, 1, 0);
	myManualMaterialObject->getTechnique(0)->setSpecular(0.3, 0.3, 0.3, 0);
	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	// Define the vertices
	manual->position(-(size / 2), -(size / 2), size / 2); // -1, -1, 1
	manual->normal(-1, 0, 0);
	manual->position(-(size / 2), -(size / 2), size / 2);
	manual->normal(0, -1, 0);
	manual->position(-(size / 2), -(size / 2), size / 2);
	manual->normal(0, 0, 1);

	manual->position((size / 2), -(size / 2), size / 2);
	manual->normal(1, 0, 0);
	manual->position((size / 2), -(size / 2), size / 2);
	manual->normal(0, -1, 0);
	manual->position((size / 2), -(size / 2), size / 2);
	manual->normal(0, 0, 1);
	
	manual->position((size / 2), (size / 2), size / 2);
	manual->normal(1, 0, 0);
	manual->position((size / 2), (size / 2), size / 2);
	manual->normal(0, 1, 0);
	manual->position((size / 2), (size / 2), size / 2);
	manual->normal(0, 0, 1);

	manual->position(-(size / 2), (size / 2), size / 2);
	manual->normal(-1, 0, 0);
	manual->position(-(size / 2), (size / 2), size / 2);
	manual->normal(0, 1, 0);
	manual->position(-(size / 2), (size / 2), size / 2);
	manual->normal(0, 0, 1);

	manual->position(-(size / 2), -(size / 2), -size / 2);
	manual->normal(-1, 0, 0);
	manual->position(-(size / 2), -(size / 2), -size / 2);
	manual->normal(0, -1, 0);
	manual->position(-(size / 2), -(size / 2), -size / 2);
	manual->normal(0, 0, -1);

	manual->position((size / 2), -(size / 2), -size / 2);
	manual->normal(1, 0, 0);
	manual->position((size / 2), -(size / 2), -size / 2);
	manual->normal(0, -1, 0);
	manual->position((size / 2), -(size / 2), -size / 2);
	manual->normal(0, 0, -1);

	manual->position((size / 2), (size / 2), -size / 2);
	manual->normal(1, 0, 0);
	manual->position((size / 2), (size / 2), -size / 2);
	manual->normal(0, 1, 0);
	manual->position((size / 2), (size / 2), -size / 2);
	manual->normal(0, 0, -1);

	manual->position(-(size / 2), (size / 2), -size / 2);
	manual->normal(-1, 0, 0);
	manual->position(-(size / 2), (size / 2), -size / 2);
	manual->normal(0, 1, 0);
	manual->position(-(size / 2), (size / 2), -size / 2);
	manual->normal(0, 0, -1);

	// Define order of vertices (define buffer)

	// Front
	manual->index(11);
	manual->index(2);
	manual->index(5);
	manual->index(11);
	manual->index(5);
	manual->index(8);

	// Back
	manual->index(23);
	manual->index(17);
	manual->index(14);
	manual->index(23);
	manual->index(20);
	manual->index(17);

	// Right
	manual->index(6);
	manual->index(3);
	manual->index(15);
	manual->index(6);
	manual->index(15);
	manual->index(18);

	// Left
	manual->index(9);
	manual->index(12);
	manual->index(0);
	manual->index(9);
	manual->index(21);
	manual->index(12);

	// Top
	manual->index(22);
	manual->index(10);
	manual->index(7);
	manual->index(22);
	manual->index(7);
	manual->index(19);

	// Bottom
	manual->index(13);
	manual->index(4);
	manual->index(1);
	manual->index(13);
	manual->index(16);
	manual->index(4);

	// End drawing
	manual->end();
	return manual;

}

ManualObject * TutorialApplication::createProceduralObject(float size, int sides)
{
	if (sides < 3) sides = 3;
	std::vector<Vector3> vertices;
	std::vector<Vector3> normalVert;

	vertices.push_back(Vector3(0, 0, 0));
	vertices.push_back(Vector3(size / 2, 0, 0));

	for (int i = 1; i < sides; ++i)
	{
		float angle = 360.0f / (float)sides;
		float x = vertices[i].x * Math::Cos(Radian(Degree(angle))) - vertices[i].y * Math::Sin(Radian(Degree(angle)));
		float y = vertices[i].x * Math::Sin(Radian(Degree(angle))) + vertices[i].y * Math::Cos(Radian(Degree(angle)));

		Vector3 vertex(x, y, 0);
		vertices.push_back(vertex);
	}

	for (int i = 0; i <= sides; ++i) {
		if (i < 2) {
			normalVert.push_back(normalize(vertices[0], vertices[1], vertices[2]));
		}
		else normalVert.push_back(normalize(vertices[0], vertices[i - 1], vertices[i]));
	}

	ManualObject* manual = mSceneMgr->createManualObject("Procedural Object");
	MaterialPtr myManualMaterialObject = MaterialManager::getSingleton().create("manualMaterial", "General");

	myManualMaterialObject->getTechnique(0)->setLightingEnabled(true);
	myManualMaterialObject->getTechnique(0)->setDiffuse(1, 1, 1, 0);
	myManualMaterialObject->getTechnique(0)->setSpecular(1, 0, 0, 0);

	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	for (int i = 0; i <= sides; ++i) {
		manual->position(vertices[i]);
		manual->normal(normalVert[i]);
	}

	for (int i = 0; i <= sides; ++i) {
		manual->index(0);
		manual->index(i + 1);
		manual->index(i + 2);
	}

	manual->index(0);
	manual->index(sides);
	manual->index(1);

	manual->end();
	return manual;
}

Vector3 TutorialApplication::normalize(Vector3 v0, Vector3 v1, Vector3 v2)
{
	Vector3 corner1 = v1 - v0;
	Vector3 corner2 = v2 - v0;
	Vector3 normal = corner1.crossProduct(corner2);
	normal.normalise();

	return normal;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{

	ManualObject* manual = createProceduralObject(10.0f, 1000.0f);
	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mObject->attachObject(manual);

	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LT_POINT);
	pointLight->setPosition(Vector3(-20.0f, 20.0f, 20.0f));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
	pointLight->setAttenuation(325.0f, 0.0f, 0.014f, 0.0007f);

}
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	mSpeed += evt.timeSinceLastFrame;


	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		mObject->translate(0, 0, -100 * evt.timeSinceLastFrame);
		mObject->translate(0, 0, -mSpeed);
	}
	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		mObject->translate(0, 0, 100 * evt.timeSinceLastFrame);
		mObject->translate(0, 0, mSpeed);
	}
	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		mObject->translate(-0.1, 0, 0);
		mObject->translate(-mSpeed, 0, 0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		mObject->translate(0.1, 0, 0);
		mObject->translate(mSpeed, 0, 0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	{
		mObject->pitch(Radian(mSpeed));
}
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	{
		mObject->pitch(Radian(-mSpeed));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
	{
		mObject->roll(Radian(-mSpeed));
	}
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	{
		mObject->roll(Radian(mSpeed));
	}

	if (!mKeyboard->isKeyDown(OIS::KC_I) && !mKeyboard->isKeyDown(OIS::KC_L) && !mKeyboard->isKeyDown(OIS::KC_J) && !mKeyboard->isKeyDown(OIS::KC_K))
	{
		mSpeed = 0;
	}
	return true;
	}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
