/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Creating of planets and setting the revolution speed
	//Sun 
	mPlanets.push_back(Planet::createPlanet(false, "sunMaterial", *mSceneMgr, 60, ColourValue(1, 1, 0)));
	mPlanets[0]->getSceneNode()->setPosition(0, 0, 0);

	//Mercury
	mPlanets.push_back(Planet::createPlanet(true, "mercuryMaterial", *mSceneMgr, 9, ColourValue(0.82f, 0.7f, 0.54f)));
	mPlanets[1]->getSceneNode()->setPosition(0, 0, -100);
	mPlanets[1]->calculateSpeed(88.0f);

	//Venus
	mPlanets.push_back(Planet::createPlanet(true, "venusMaterial", *mSceneMgr, 15, ColourValue(0.93f, 0.9f, 0.67f)));
	mPlanets[2]->getSceneNode()->setPosition(0, 0, -150);
	mPlanets[2]->calculateSpeed(224.0f);

	//Earth
	mPlanets.push_back(Planet::createPlanet(true, "earthMaterial", *mSceneMgr, 30, ColourValue::Blue));
	mPlanets[3]->getSceneNode()->setPosition(0, 0, -200);
	mPlanets[3]->calculateSpeed(365.0f);

	//Moon
	mPlanets.push_back(Planet::createPlanet(true, "moonMaterial", *mSceneMgr,3, ColourValue(0.7f, 0.7f, 0.7f)));
	mPlanets[4]->getSceneNode()->setPosition(0, 0, -225);
	mPlanets[4]->calculateSpeed(5.0f);

	//Mars
	mPlanets.push_back(Planet::createPlanet(true, "marsMaterial", *mSceneMgr, 24, ColourValue(0.71f,0.25f,0.05f)));
	mPlanets[5]->getSceneNode()->setPosition(0, 0, -250);
	mPlanets[5]->calculateSpeed(687.0f);

	Light* light = mSceneMgr->createLight();
	light->setType(Light::LightTypes::LT_POINT);
	light->setPosition(Vector3(0, 0, 0));
	light->setDiffuseColour(ColourValue::White);
	light->setSpecularColour(1, 1, 1);
	light->setAttenuation(3250, 1.0, 0.0014, 0.000007);
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	// Rotation of the planets, moon, and sun
	for (int c = 0; c <= 5; c++)
	{
		mPlanets[c]->rotate(evt.timeSinceLastFrame);
	}

	// Revolution of the planets around the sun
	// Mercury
	mPlanets[1]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);
	// Venus
	mPlanets[2]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);
	// Earth
	mPlanets[3]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);
	// Mars
	mPlanets[5]->orbit(mPlanets[0]->getSceneNode(), evt.timeSinceLastFrame);

	// Revolution of the moon around the earth
	mPlanets[4]->orbit(mPlanets[3]->getSceneNode(), evt.timeSinceLastFrame);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
