#include "Planet.h"



Planet::Planet(SceneNode* sceneNode)
{
	mSceneNode = sceneNode;
}

Planet::~Planet()
{
}

SceneNode * Planet::getSceneNode()
{
	return mSceneNode;
}

Planet * Planet::createPlanet(bool affectedByLight, std::string materialName, SceneManager & sceneManager, float size, ColourValue colour)
{
	float sides = 30.0f;
	// Ask the scene manager to create an object for us
	std::vector<Vector3> vertices;
	std::vector<Vector3> normalVert;

	vertices.push_back(Vector3(0, size / 2, 0));

	// Creating of sphere:
	// For every side, the point stops and creates a circle and then proceeds to the next side until a sphere is fully created.
	for (int i = 0; i <= sides; ++i)
	{
		// The side where the point stops.
		vertices.push_back(zRotation(Vector3(vertices[i * sides])));
		// A circle is created after the point stops.
		for (int j = 1; j < sides; ++j)
		{
			vertices.push_back(yRotation(Vector3(vertices[i*sides + j])));
		}
	}

	ManualObject* manual = sceneManager.createManualObject();
	MaterialPtr myManualMaterialObject = MaterialManager::getSingleton().create(materialName, "General");
	myManualMaterialObject->getTechnique(0)->setLightingEnabled(affectedByLight);
	myManualMaterialObject->getTechnique(0)->setDiffuse(colour);
	myManualMaterialObject->getTechnique(0)->setSpecular(0.3, 0.3, 0.3, 0);
	manual->begin(materialName, RenderOperation::OT_TRIANGLE_LIST);

	// Normalize the vertices
	for (int i = 0; i <= sides * sides; ++i)
	{
		manual->position(vertices[i]);
		Vector3 vect(vertices[i].normalisedCopy());
		manual->normal(vect);
		manual->colour(colour);
	}

	// Top side of the sphere
	for (int i = 1; i <= sides; ++i)
	{
		manual->index(0);
		manual->index(i);
		manual->index(i + 1);
	}

	// Connecting the indexes to form quads
	for (int i = 0; i <= sides * sides; ++i)
	{
		manual->index(i + 30);
		manual->index(i + 1);
		manual->index(i);
		manual->index(i + 30);
		manual->index(i + 31);
		manual->index(i + 1);
	}
	manual->end();
	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode();
	node->attachObject(manual);
	Planet* planet = new Planet(node);

	return planet;
}

void Planet::calculateSpeed(float speed)
{
	// Formula for the revolution of planets
	float pi = 3.141592654f;
	mOrbitSpeed = 365.0f / speed;
	mOrbitSpeed *= (6.0f*pi) / 180.0f;
}

void Planet::rotate(float speed)
{
	mSceneNode->rotate(Ogre::Vector3(0, 1, 0), Radian(speed));
}

void Planet::orbit(SceneNode * origin, float speed)
{
	float newX = (mSceneNode->getPosition().x - origin->getPosition().x) * cos(speed * mOrbitSpeed) + (mSceneNode->getPosition().z - origin->getPosition().z) * sin(speed * mOrbitSpeed);
	float newZ = (mSceneNode->getPosition().x - origin->getPosition().x) * -sin(speed * mOrbitSpeed) + (mSceneNode->getPosition().z - origin->getPosition().z) * cos(speed * mOrbitSpeed);

	// Allocating the new coordinates into a variable. Y remains the same.
	float x = newX + origin->getPosition().x;
	float y = mSceneNode->getPosition().y;
	float z = newZ + origin->getPosition().z;

	// Setting the new coordinates
	mSceneNode->setPosition(x, y, z);
}

Vector3 Planet::zRotation(Vector3 variables)
{
	float newZ = 0;
	float newX = variables.x * Math::Cos(Radian(Degree(6.0f))) - variables.y * Math::Sin(Radian(Degree(6.0f)));
	float newY = variables.x * Math::Sin(Radian(Degree(6.0f))) + variables.y * Math::Cos(Radian(Degree(6.0f)));
	return Vector3(newX, newY, newZ);
}

Vector3 Planet::yRotation(Vector3 variables)
{
	float newY = variables.y;
	float newX = variables.x * Math::Cos(Radian(Degree(12.0f))) + variables.z * Math::Sin(Radian(Degree(12.0f)));
	float newZ = variables.x * -Math::Sin(Radian(Degree(12.0f))) + variables.z * Math::Cos(Radian(Degree(12.0f)));
	return Vector3(newX, newY, newZ);
}