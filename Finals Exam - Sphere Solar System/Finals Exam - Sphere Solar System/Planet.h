#pragma once
#include "BaseApplication.h"

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* sceneNode);
	~Planet();
	SceneNode* getSceneNode();
	static Planet* createPlanet(bool affectedByLight, std::string materialName, SceneManager &sceneManager, float size, ColourValue colour);
	void calculateSpeed(float speed);
	void rotate(float speed);
	void orbit(SceneNode* origin, float speed);

private:
	float mOrbitSpeed;
	SceneNode* mSceneNode;
	static Vector3 zRotation(Vector3 variables);
	static Vector3 yRotation(Vector3 variables);
};

